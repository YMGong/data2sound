# The 800,000-year-old sound 

This Project contains Matlab scripts that convert data into audio sound. 

[code](https://gitlab.com/YMGong/data2sound/tree/master/code) contains the Matlab scripts to convert the data to sound.

[data](https://gitlab.com/YMGong/data2sound/tree/master/data) contains the Antarctic ice-core records of carbon dioxide (CO2).

You can listen to the [‘raw’ wave](https://soundcloud.com/yongmei-gong/icecoresound-wave/s-dZ2vK?si=ba11e59fb406477d9ffc3ba1b0d9600c&utm_source=clipboard&utm_medium=text&utm_campaign=social_sharing) and the ‘translation’ to nodes played with [synthetic sound](https://soundcloud.com/yongmei-gong/icecoresound-synthetic/s-tsPwy?si=79d66464071c41329a07573063b77341&utm_source=clipboard&utm_medium=text&utm_campaign=social_sharing) on soundcloud.

 
# Introduction
Sound is a vibration that typically propagates as an audible wave of pressure, through a transmission medium such as a gas, liquid or solid.   

When an object vibrates it also pushes the air to move (changes the air pressure). The initial movement (the vibration) of the air can be transmitted and propagated in space since the space we live into on earth is filled with air until it reaches the eardrum. The eardrum passes the vibrations through the middle ear bones or ossicles into the cochlea in the inner ear. Inside the cochlea, there are thousands of tiny hair cells. Hair cells change the vibrations into electrical signals that are sent to the brain through the hearing nerve. The brain tells you that you are hearing a sound and what that sound is. So that we can say sound is the reception of such waves and their perception by the brain.  The human ears can only hear sound waves as distinct pitches when the frequency lies between about 20 Hz and 20 kHz.   

In this project the Antarctic ice-core records of CO2 (carbon dioxide) concentrition over the last 800,000 years is provided as an example to convert data to sound. 

A popular science article about ice cores drilling and the records can be found here:
[Ice Cores “For Dummies”](https://blogs.egu.eu/divisions/cr/2016/12/14/ice-cores-for-dummies/)  ( Please read ‘Atmospheric gas’ for information directly related to the record of CO2 concentration).

The wave-like distribution of the ice core data (CO2 concentration in this case) oscillates periodically in time. It reflects the cyclic changes of the earth’s orbits around the sun. These cycles combine to affect the amount of solar heat that’s incident on the earth’s surface and subsequently influence climatic patterns, thus the CO2 concentration in the atmosphere as a consequence.

![](Pictures/Milankovitch_cycles.png)


*[The Milankovitch cycles](https://www.universetoday.com/39012/milankovitch-cycle/)*

# Data

<details>
<summary> </summary>
This section introduces the Antarctic ice-core records of carbon dioxide (CO2) used to produce the two sound tracks. The record has been extended back to 800,000 years at Dome C and over 400,000 years at the Vostok site. The 2000-year record from Law Dome, Antarctica, has been merged with modern records to provide a 2000-year time series extending to the present. 

![](Pictures/Ice_core_drilling_locations.png)

*Maps of Antarctica showing locations and elevations in meters above sea level (masl) of: Law Dome (66°44'S, 112°50'E, 1390 masl), Dome C (75°06'S, 123°24'E, 3233 masl), Taylor Dome (77°48'S, 158°43'E, 2365 masl), Vostok (78°28'S, 106°52'E, 3500 masl), Dome A (80°22'S, 77°22'E, 4084 masl), the South Pole station (90°S, 2810 masl), and Siple Station (75°55'S, 83°55'W, 1054 masl)*

The data is donwloaded from [Carbon Dioxsite Information Analysis Center (CDIAC)](https://cdiac.ess-dive.lbl.gov/trends/co2/ice_core_co2.html).

The collective period of the record is ~137 to 795,000 years Before Present (B.P.) (Before year 1950).

Composite of the full record built from the following: 

| Age            | Location           | Reference                     |
| ------         | ------             | ------                        |
| -51-1800 yr BP | Law Dome           | Rubino et al., 2013           |
| 1.8-2 kyr BP   | Law Dome           | MacFarling Meure et al., 2006 |
| 2-11 kyr BP    | Dome C             | Monnin et al., 2001 and 2004  |
| 11-22 kyr BP   | WAIS               | Marcott et al., 2014          |
| 22-40 kyr BP   | Siple Dome         | Ahn et al., 2014              |
| 40-60 kyr BP   | TALDICE            | Bereiter et al., 2012         |
| 60-115 kyr BP  | EDML               | Bereiter et al., 2012         |
| 105-155 kyr BP | Dome C Sublimation | Schneider et al., 2013        |
|155-393 kyr BP  | Vostok             | Petit et al., 1999            |
|393-611 kyr BP  | Dome C             | Siegenthaler et al., 2005     |
|612-800 kyr BP  | Dome C             | Bereiter et al., 2014         |

Over the last 800,000 years atmospheric CO2 levels as indicated by the ice-core data have fluctuated between 170 and 300 parts per million by volume (ppmv), corresponding with conditions of glacial and interglacial periods. The Vostok core indicates very similar trends. Prior to about 450,000 years before present time (BP) atmospheric CO2 levels were always at or below 260 ppmv and reached lowest values, approaching 170 ppmv, between 660,000 and 670,000 years ago. The highest pre-industrial value recorded in 800,000 years of ice-core record was 298.6 ppmv, in the Vostok core, around 330,000 years ago. Atmospheric CO2 levels have increased markedly in industrial times; measurements in year 2010 at Cape Grim Tasmania and the South Pole both indicated values of 386 ppmv and are currently increasing at about 2 ppmv/year.

![](Pictures/CO2_800kyrBP.jpg)

*CO2 concerntration from 800,000 BP to 2001*

</details>

# Method 

<details>
<summary> </summary>

This section briefly describes the method and tool used to convert the data into sound that can be played by the speaker. 

## Software

A mathematical computing software [Matlab](https://nl.mathworks.com/?s_tid=gn_logo) from MathWork is used to convert the ice core data into sound.  
The function [sound (data, Fs)](https://nl.mathworks.com/help/matlab/ref/sound.html) in the main script [`d2s.m`](https://gitlab.com/YMGong/data2sound/blob/master/code/d2s.m) sends audio signal (data) to the speaker at a certain sample rate (Fs).

## Data as the audio signal 

The first part of the code ‘concentrate’ the data distribution in time from year to second and converts the signal to audio sound. In this audio file audiences directly listen to the ‘vibration' or ‘disturbance’ created by the changes of the CO2 concentration from the year 800, 000 bp. to 2001. 

## Data as frequency of sine wave signal 

The second part of the code uses the value of the CO2 concentration as the frequency of sine wave signal to make synthetic sounds.  A 220Hz sine wave sounds like [this](https://en.wikipedia.org/wiki/File:220_Hz_sine_wave.ogg). In this audio file audiences listen to the synthetic sine sound track of which each note has the frequency of the value of the CO2 concentration from the year 800, 000 bp. to 2001. 

![](Pictures/Sine_wave.png)

*Sine wave*

</details>
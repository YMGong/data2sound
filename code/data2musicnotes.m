function y=data2musicnotes(data, samplstep, p, Tinterv, Fs, amplifier)
% Fs - sample rate control the quality
% T - time of every note
% data - should be the input frequency, corresponds to the pitch of music
%        notes
t=0:1/Fs:Tinterv;%s, time array
y = zeros(1,length(t)); 
for n=1:length(p)
    y = y + p(n)*sin(2*pi*n*data(1,1)*t); % sythesize waveform
end
%y=amplifier*(y/max(y)); %initialise the sound with the first data value
y=amplifier*y;
for i=2:samplstep:length(data)
    f = data(i);
    %y=[y, amplifier*sin(2*pi*f*t)];
    y_tmp = zeros(1,length(t)); 
    for n=1:length(p)
        y_tmp = y_tmp + p(n)*sin(2*pi*n*f*t); % sythesize waveform
    end
    %y = [y, amplifier*(y_tmp/max(y_tmp))]; 
    y = [y, amplifier*y_tmp]; 
  
end
% figure,
% plot(t,y);
end
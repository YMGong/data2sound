%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Generating sound from ice core data            %%%
%%% yongmei.gong.ac@outlook.com                    %%%
%%% 22 Dec 2108                                    %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%This is created from 800,000-year CO2 concentration data measured from several Antarctica ice cores. The sound is generated through Matlab code according to the 'wavy' shape of the data distribution.

close all;
clear all;
addpath /Users/yongmei/Documents/Academy/Project/ICECORESOUND/data2sound/data
%%%
% check the data for the first time 
%%%

%filename = 'antarctica2015co2byrd.txt';
%temp=uiimport(filename); % when you are not sure about how to import the
                          % text data here interactively check the number
                          % of the headlines and the delimiter. Then import
                          % them as mat data.
                         

%% loading data
%%%

% run this after you creat the mat data
%%{
dinfo = dir('/Users/yongmei/Documents/Academy/Project/ICECORESOUND/data2sound/data/*.mat');
for i=1:length(dinfo)
    load(dinfo(i).name)
end
% arrange the age and co2_ppm
data_tmp(:,1)=[antarctica2015co2law(:,1);...
    antarctica2015co2lawsp(:,2);...
    antarctica2015co2byrd(:,2);...
    antarctica2015co2composite(:,1);...
    antarctica2015co2domec(:,2);...
    antarctica2015co2edc(:,2);...
    antarctica2015co2edml(:,2);...
    antarctica2015co2siple(:,2);...
    antarctica2015co2talos(:,2);...
    antarctica2015co2taylor(:,2);...
    antarctica2015co2vostok(:,2);...
    antarctica2015co2wais(:,2)];
data_tmp(:,2)=[antarctica2015co2law(:,2);...
    antarctica2015co2lawsp(:,3);...
    antarctica2015co2byrd(:,4);...
    antarctica2015co2composite(:,2);...
    antarctica2015co2domec(:,4);...
    antarctica2015co2edc(:,4);...
    antarctica2015co2edml(:,4);...
    antarctica2015co2siple(:,4);...
    antarctica2015co2talos(:,5);...
    antarctica2015co2taylor(:,3);...
    antarctica2015co2vostok(:,4);...
    antarctica2015co2wais(:,3)];
data_tmp2=data_tmp(~isnan(data_tmp(:,1)),:);
data_tmp3=data_tmp2(~isnan(data_tmp2(:,2)),:);
% [data(:,1),index]=sort(data_tmp3(:,1));
% data(:,2)=data_tmp3(index,2);
[data(:,1),IA,IC] = unique(data_tmp3(:,1),'legacy'); %for interpolation later
data(:,2)=data_tmp3(IA,2);
data=flipud(data); %oldest comes first
plot(data(:,1)/1000.0,data(:,2),'-k','LineWidth',0.7);hold on;
%plot(data_inter(:,1),(data(:,2)-data(1,2))/max(data(:,2)),'-.r');
ylabel('CO_2 ppm','FontSize', 15);
xlabel('kyr BP');
ax = gca;
set(gca, 'xlim',[-1e2 9e2])
set(gca, 'XDir','reverse','LineWidth',1.5)
ax.FontSize=15;
ax.YGrid='on';
data_ori = data;
 % from the year 105
 
%% Data as the audio signal
%%%
% Now convert the age and data
%%%

time2sec=60.0/8;        % the time scale is seconds 
                        % 60.0/4 means quarter a min
Fs=8000;                % sample rate
ref = data(1,2);        % use the oldest data as reference
%ref = mean(data(:,2)); % you can also use the mean
amplifier = 2;         % tuning the volume
nbites = 16;
data(:,1)=(abs(data(:,1)-data(1,1)))/1e5*time2sec; % we normalize the time 
                                                   % and conver it into seconds
                                                   % now we have about 2
                                                   % minites of sounds
% interpolating to get more data points
data_inter(:,1)=(data(1,1):1/Fs:data(end,:));
data_inter(:,2)=interp1(data(:,1),data(:,2),data_inter(:,1));
data=data_inter;

data_sound = (data(:,2)-ref)/100;%/max(data(:,2)); % some you can't put it to be too small otherwise the volume is too small
% normalise the data as the inputs are assumed to be in the range -1.0 <= y <= 1.0. 
%data_sound = data_sound + abs(min(data_sound));
figure, plot(data_sound)

%data_sound = data(:,2);                     % Values outside that range are clipped. 
%sound(amplifier*data_sound,Fs);
% data_soundInt = data_sound * 32768;
% data_soundInt(data_soundInt == 32768) = 32767;
% data_sound(data_sound > 1) =1;
audiowrite('../sound_files/icecoresound_wave.mp4', data_sound, Fs, 'BitRate', nbites);
% don't write wav file
%%}


%% Use data as the frequency for the synthetic notes
%%%

%%{
Fs=44000;                       % sample rate, control the quality
Tinterv=0.3;%0.2;                   % s, time length of very note
samplstep = 1;                 % decide the step size to take data value from the array
amplifier = 0.008;                 % control the volume
nbites = 24;                   % bite rate, control the quality of the sound

%p = [1 .8 .04];               % control the sound 'texture'
p = 1;                         % 1 is sine wave I like it!
%p = [1 .8 .04 .8 .2]; 

%data_input = [220,440, 352, 346, 220, 350];       % it is better to use numbers that can be divided by 10
%data_input = data_ori(end-100:end,2);   %try the ice core data
data_input = data_ori(:,2);

y = data2musicnotes(round(data_input/10)*10, samplstep, p, Tinterv, Fs, amplifier);
%sound(y,Fs,nbites);
audiowrite('icecoresound_synthetic.wav', y, Fs);%, 'BitRate', nbites);
%%}

%{
%%%
% now make some synthesitic sound
%%%
length(data_ori)
y=0;
%data_f=data_sound;
for i=1:10:length(data_ori(:,2))
%f = 220*2^(data_f(i)/12); %Hz
f = data_ori(i,2);
y= [y,synthesize_f(f, 0.4, [1 .8 .1 .04])];    
     
end
Fs=22050; nbits=8; 
sound(y,Fs,nbits)
%}
%}